1. http://devdocs.magento.com/guides/v2.0/install-gde/prereq/connect-auth.html

Using the above guide, generate your access keys in order to use repo.magento.com - or use mine:

- Public Key: 20c336e4379e5e9703dc8f9f4fb1cbda
- Private Key: a98d731c4bb00d80e8697a6f7a35994c


2. Taken from http://devdocs.magento.com/guides/v2.0/install-gde/prereq/integrator_install_ce.html and http://devdocs.magento.com/guides/v2.0/install-gde/install/cli/install-cli-install.html :

Once you have access keys, enter the following command from your web directory, substituting directory_name for the name of your choice (usually the site name):
```
composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition directory_name/
```
Or for a specific version (e.g. 2.1.7):
```
composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition=2.1.7 directory_name/
```
When prompted, your username is your public key and password is your private key (from the step above)

3. Then use the following information to set the correct file permissions (substitute www-data for admin or equivalent apache server user on a mac):

```
cd <your Magento install dir>
find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;
find ./var -type d -exec chmod 777 {} \;
find ./pub/media -type d -exec chmod 777 {} \;
find ./pub/static -type d -exec chmod 777 {} \;
chmod 777 ./app/etc var/ generated/
chmod 644 ./app/etc/*.xml
chown -R :www-data .
chmod u+x bin/magento
```

4. Once this is run, the command line is used to install and set up the magento project. The simplest way is to
```
cd <your Magento install dir>
```

and run commands as

```
bin/magento <command name>
```
depending on your system setup you may need to run commands as

```
php bin/magento <command name>
```

A full list of commands is available in the documentation here: http://devdocs.magento.com/guides/v2.0/install-gde/install/cli/install-cli-subcommands.html#instgde-cli-summary

5. Create a database for the site (using balti2.com as an example site - replace all instances of this in the setup commands etc. with the correct name of the site being created)

```
mysql -u root -p
create database harrisonsm2_mig;
exit
```

6. First command to run is as follows. This will allow server rewrites, set up a default admin user and password (can be changed to suit your preferences)

```
php bin/magento setup:install --base-url=https://harrisonsm2-blank.icansee/ \
--db-host=localhost --db-name=harrisonsm2_mig \
--db-user=root --db-password='j#opWSsF8nB9UC9JGJsfy*XGW9jqVudt' \
--admin-firstname=David --admin-lastname=OConnor --admin-email=david.oconnor@icansee.co.uk \
--admin-user=icsadmin --admin-password=1csmail23 --language=en_US \
--currency=GBP --timezone=Europe/London --cleanup-database \
--sales-order-increment-prefix="ORD$" --session-save=files --use-rewrites=1 --use-secure=1 --use-secure-admin=1 --base-url-secure=https://harrisonsm2-blank.icansee/
```

If the command runs successfully, you will see similar to the following messages:
```
[SUCCESS]: Magento installation complete.
[SUCCESS]: Magento Admin URI: /admin_and2tz
Nothing to import.
```

The admin URI is the url for the admin panel. Copy this down (also is in app/etc/env.php after installing magento)

7. Set up your hosts file, apache .conf file and any SSL certificates, run a2ensite and service apache2 reload and access the site. If you have a 500 error follow this guide here: http://devdocs.magento.com/guides/v2.1/config-guide/prod/prod_file-sys-perms.html (usually a file permission issue)

For development and to see all errors, put the installation into developer mode:

```
php bin/magento deploy:mode:set developer
```

## To clear caches and generated files: ##

```
rm -rf var/cache/* var/page_cache/* var/di/* var/generation/* generated/* var/view_preprocessed/*
```
For static content also
```
rm -rf pub/static/*
```
And
```
php bin/magento cache:clean
php bin/magento cache:flush
```

## To import sample data for testing ##

```
php bin/magento sampledata:deploy
```
This means no db dumps are required - security and data protection safe that way! (recommended only for working out how magento works - documentation says not to use sample data for development)

## Reset admin password: ##

```
mysql -u <user> -p <database>
```
Then
```
UPDATE admin_user SET password = CONCAT(SHA2('ghsi654ksomesecurepassword789', 256), ':ghsi654k:somesecurepassword789') WHERE username = 'icsadmin';
```
Where ghsi654k isa salt used for the password hash - create your own here, 8 alphanumeric characters, and somesecurepassword789 is your new admin password
## Create admin user: ##

```
php bin/magento admin:user:create \
    --admin-user="god" \
    --admin-password="1csmail23" \
    --admin-email="god@the-universe.com" \
    --admin-firstname="God" \
    --admin-lastname="Almighty"
```

## Uninstall a manually added theme: ##

If you created or manually installed the theme, it does not matter how you installed Magento.

To uninstall a manually added theme:

- Navigate to the vendor directory where the theme was installed. This directory should be:
```
<Magento root dir>/app/design/frontend/<VendorName>
```
- Remove the theme directory.
- Remove the theme record from database. If you are using MySQL, run the following command to do this:
```
mysql -u <user> -p -e "delete from <dbname>.theme where theme_path ='<Vendor>/<theme>' AND area ='frontend' limit 1"
```
Where:

- <user>: your Magento database user name
- <dbname>: your Magento database name
- <Vendor>/<theme>: relative path to the theme directory

```
UPDATE core_config_data SET `value` = 1 WHERE path = 'design/theme/theme_id';
```

#Error Warnings#

Below are various common error warnings and their fixes.

1. There has been an error processing your request

###Exception printing is disabled by default for security reasons.###

Error log record number: 962744043178

This log is stored in var/report/962744043178 so check the log to find out.

2. Cannot create/write to directory /path/to/magento/generated/... or /path/to/magento/var/cache...

Incorrect file ownership and/or permissions on specified directory. Web server user needs write permissions on these directories (occasioinally also app/etc/env.php)
```
sudo chown -R <web server user>:<web server user> var/cache/ generated/ pub/static/  
```

3. Unable to retrieve static content for...

You are in production mode. Either:
```
php bin/magento deploy:mode:set developer
```
Or:
```
php bin/magento setup:di:compile
```

4. Unable to retrieve deployment version of static files from the file system
```
setup:static-content:deploy
```

5. After running php bin/magento cache:clean (or any other command like this)
[InvalidArgumentException]
  There are no commands defined in the "cache" namespace.
```
sudo chown -R www-data:www-data var/
```
Where www-data is the web server user.
